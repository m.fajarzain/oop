<?php
    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');

    $sheep = new Animal("Shawn");
    echo "Nama : ".$sheep->nama. "<br>";
    echo "Kaki : ".$sheep->legs. "<br>";
    echo "Berdarah Dingin : ".$sheep->cold_blooded. "<br>";
    echo "<br>";

    $kodok = new Frog("Buduk");
    echo "Nama : ".$kodok->nama. "<br>";
    echo "Kaki : ".$kodok->legs. "<br>";
    echo "Berdarah Dingin : ".$kodok->cold_blooded. "<br>";
    echo $kodok->Jump();
    echo "<br><br>";

    $sungokong = new Ape("Sungokong");
    echo "Nama : ".$sungokong->nama. "<br>";
    echo "Kaki : ".$sungokong->legs. "<br>";
    echo "Berdarah Dingin : ".$sungokong->cold_blooded. "<br>";
    echo $sungokong->Yell();
    echo "<br><br>";
  
?>